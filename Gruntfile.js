module.exports = function(grunt) {

    var date = grunt.template.today('yyyy-mm-dd HH:MM:ss Z');
    var year = grunt.template.today('yyyy');

    var fn = '<%= pkg.name %>-<%= pkg.version %>';
    var banner_long = '/*!\n * <%= pkg.title %> version <%= pkg.version %> | ' + date + '\n * <%= pkg.homepage %>\n * \n * Copyright 2013-' + year + ' <%= pkg.author %>\n */\n\n\n\n';
    var banner_short = '/*! <%= pkg.title %> version <%= pkg.version %> (' + date + ') <%= pkg.homepage %> Copyright 2013-' + year + ' <%= pkg.author %> | Including Sizzle by jQuery Foundation, Inc. jquery.org/license */\n';

    var pkg = grunt.file.readJSON('package.json');

    grunt.initConfig({
        pkg: pkg,
        includes: {
            target: {
                files: [{
                    src: ['src/main.js'],
                    dest: 'tmp/jas.js',
                    flatten: true
                },{
                    src: ['src/css/main.css'],
                    dest: 'tmp/jas.css',
                    flatten: true
                }]
            }
        },
        comments: {
            options: {
                singleline: true,
                multiline: true
            },
            src: ['tmp/jas.js', 'tmp/jas.css']
        },
        template: {
            def: {
                files: [{
                    src: ['tmp/jas.js'],
                    dest: 'tmp/jas.js',
                },{
                    src: ['tmp/jas.css'],
                    dest: 'tmp/jas.css',
                }],
                options: {
                    data: {
                        pkg: pkg,
                    }
                }
            }
        },
        cssmin: {
            files: {
                src: 'tmp/jas.css',
                dest: 'tmp/jas.min.css'
            }
        },
        uglify: {
            files: {
                src: 'tmp/jas.js',
                dest: 'tmp/jas.min.js'
            }
        },
        jsbeautifier: {
            files: ['tmp/jas.js'],
        },
        file_append: {
            target: {
                files: [
                    {
                      prepend: banner_long,
                      input: 'tmp/jas.js',
                      output: 'tmp/jas.js',
                    },{
                      prepend: banner_short,
                      input: 'tmp/jas.min.js',
                      output: 'tmp/jas.min.js',
                    }
                ]
            }
        },
        copy: {
            target: {
                files: [
                    {src: ['tmp/jas.js'], dest: 'build/jas.js'},
                    {src: ['tmp/jas.min.js'], dest: 'build/jas.min.js'},
                    {src: ['tmp/jas.js'], dest: 'build/' + fn + '/' + fn + '.js'},
                    {src: ['tmp/jas.min.js'], dest: 'build/' + fn + '/' + fn + '.min.js'},
                ],
            },
        },
    });

    grunt.loadNpmTasks('grunt-includes');
    grunt.loadNpmTasks('grunt-stripcomments');
    grunt.loadNpmTasks('grunt-template');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-jsbeautifier');
    grunt.loadNpmTasks('grunt-file-append');
    grunt.loadNpmTasks('grunt-contrib-copy');

    grunt.registerTask('load_css_data', 'Loading jas.min.css', function() {
        var css_data = grunt.file.read('tmp/jas.min.css');
        var js = grunt.file.read('tmp/jas.js');
        js = js.replace('{{css_data}}', css_data);
        grunt.file.write('tmp/jas.js', js);
    });

    grunt.registerTask('default', [
        'includes',
        'comments',
        'template',
        'cssmin',
        'load_css_data',
        'uglify',
        'jsbeautifier',
        'file_append',
        'copy',
    ]);

    grunt.registerTask('dev', [
        'includes',
        'template',
        'cssmin',
        'load_css_data',
        'copy',
    ]);

};
