include "cookies.js"

_.escape_html = function (text) {
  text = text + '';
  var map = {
    	'&': '&amp;',
    	'<': '&lt;',
    	'>': '&gt;',
    	'"': '&quot;',
    	"'": '&#039;',
  	};
    return text.replace(/[&<>"']/g, function(m) { return map[m]; });
};

_.cbrt = function (n) {
    var r = Math.pow(Math.abs(n), 1 / 3);
    return n < 0 ? -r : r;
};

_.new_element = function (tagname, attributes, parent) {
    var e = doc.createElement(tagname);
    for (var k in attributes) {
        if (attributes.hasOwnProperty(k)) {
            e.setAttribute(k, attributes[k]);
        }
    }
    if (typeof parent !== 'undefined') {
        parent.appendChild(e);
    }
    return e;
};

_.ajax = function (a, f) {
    a.request = new XMLHttpRequest();
    a.orsc = function () {
        switch (this.request.readyState) {
            case 4:
                this.result(this.request.responseText, this.request.status);
                break;
        }
    };
    a.request.onreadystatechange = function () {
        a.orsc();
    };
    a.request.open(a.method, a.url, true);
    var fd;
    var formdata = (typeof a.formdata !== 'undefined') ? a.formdata : false;
    if (formdata) {
        fd = a.data;
    } else {
        fd = new FormData();
        for (var k in a.data) {
            if (a.data.hasOwnProperty(k)) {
                fd.append(k, a.data[k]);
            }
        }
    }
    a.request.send(fd);
};

_.cubic_bezier_parse = function (s) {
    switch (s) {
        case 'ease':
            return [.25, .1, .25, 1];
        case 'linear':
            return [0, 0, 1, 1];
        case 'ease-in':
            return [.42, 0, 1, 1];
        case 'ease-out':
            return [0, 0, .58, 1];
        case 'ease-in-out':
            return [.42, 0, .58, 1];
        default:
            if (s.indexOf('cubic-bezier(') === 0) {
                var t = s.substr(13);
                t = t.substr(0, t.indexOf(')'));
                t = t.split(',');
                for (var i = 0; i < 4; i++) {
                    t[i] = parseFloat(t[i]);
                }
                return t;
            }
    }
    return false;
};

_.cubic_bezier_math = function (bz, x) {
    if ((x === 0) || (x === 1))
        return x;
    var a = 3 * bz[0] + 1 - 3 * bz[2];
    var b = -6 * bz[0] + 3 * bz[2];
    var c = 3 * bz[0];
    var d = -x;
    var p = (3 * a * c - Math.pow(b, 2)) / (3 * Math.pow(a, 2));
    var q = (2 * Math.pow(b, 3) - 9 * a * b * c + 27 * Math.pow(a, 2) * d) / (27 * Math.pow(a, 3));
    var r = Math.pow(p / 3, 3) + Math.pow(q / 2, 2);
    var t;
    if (r < 0) {
        var f;
        if (q < 0) {
            f = Math.atan(Math.sqrt(-r) / (-q / 2));
        } else if (q > 0) {
            f = Math.atan(Math.sqrt(-r) / (-q / 2)) + Math.PI;
        } else {
            f = Math.PI / 2;
        }
        var b1 = 2 * Math.sqrt(-p / 3);
        var b2 = b / (3 * a);
        t = b1 * Math.cos(f / 3) - b2;
        if ((t < 0) || (t > 1))
            t = b1 * Math.cos(f / 3 + (2 * Math.PI) / 3) - b2;
        if ((t < 0) || (t > 1))
            t = b1 * Math.cos(f / 3 + (4 * Math.PI) / 3) - b2;
    } else if (r > 0) {
        t = _.cbrt(-q / 2 + Math.sqrt(r)) + _.cbrt(-q / 2 - Math.sqrt(r)) - b / (3 * a);
    } else {
        t = -_.cbrt(-q / 2);
        if ((t < 0) || (t > 1))
            t = t * -2;
    }
    var b1 = 3 * t * Math.pow(1 - t, 2);
    var b2 = 3 * Math.pow(t, 2) * (1 - t);
    var b3 = Math.pow(t, 3);
    var y = b1 * bz[1] + b2 * bz[3] + b3;
    return y;
};

var go22_f, go22_run, go22_timer, go22_counter, go22_total, go22_sp, go22_fp, go22_last;

_.page_scroll = function (d) {
    var t = (typeof d.speed !== 'undefined') ? d.speed : 500;
    go22_f = _.cubic_bezier_parse((typeof d.timing_function !== 'undefined') ? d.timing_function : 'ease');
    if (typeof go22_run !== 'undefined') {
        if (go22_run)
            clearInterval(go22_timer);
    }
    go22_run = true;
    go22_counter = go22_total = Math.floor(t / 10) + 1;
    go22_sp = Math.floor(doc.documentElement.scrollTop || doc.body.parentNode.scrollTop || doc.body.scrollTop);
    go22_fp = ((typeof d.target !== 'undefined') ? doc.querySelector(d.target).getBoundingClientRect().top + go22_sp : ((typeof d.pos !== 'undefined') ? d.pos : 0)) + ((typeof d.offset !== 'undefined') ? d.offset : 0);
    var f = function () {
        var y = go22_sp + (go22_fp - go22_sp) * _.cubic_bezier_math(go22_f, 1 - (go22_counter / go22_total));
        go22_last = Math.floor(y);
        if (doc.documentElement)
            doc.documentElement.scrollTop = go22_last;
        if (doc.body.parentNode)
            doc.body.parentNode.scrollTop = go22_last;
        if (doc.body)
            doc.body.scrollTop = go22_last;
        go22_counter--;
        if (go22_counter < 0) {
            clearInterval(go22_timer);
            go22_run = false;
        }
    };
    f();
    go22_timer = setInterval(f, 10);
};

_.get = function (name, default_value) {
    if (get_params === null) {
        get_params = {};
        var query = doc.location.search.substring(1);
        var pairs = query.split('&');
        var pair;
        for (var i = 0; i < pairs.length; i++) {
            pair = pairs[i].split('=');
            get_params[pair[0]] = decodeURIComponent(pair[1]);
        }
    }
    if (typeof default_value === 'undefined') {
        default_value = null;
    }
    if (typeof get_params[name] === 'undefined') {
        return default_value;
    } else {
        return get_params[name];
    }
};

_.defr = function (a, b) {
    for (var k in b) {
        if (b.hasOwnProperty(k)) {
            if (typeof a[k] === 'undefined') {
                a[k] = b[k];
            }
        }
    }
};

_.time = function () {
    return (new Date()).getTime();
};
