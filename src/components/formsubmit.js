_.type('formsubmit');

_.proc('created', function () {
    this.jas_inherit();
    this.jas_set.class_add('jas-formsubmit');
    this.input = _.new_element('input', {
        type: 'submit'
    }, this);
}, 'formsubmit');

/*
_.proc('kill', function() {
    this.jas_inherit();
}, 'formsubmit');
*/
