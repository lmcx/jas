_.type('form');

_.proc('created', function () {
    this.jas_inherit();
    this.jas_set.class_add('jas-form');
    this.method = this.jas_set.attr_get('method', 'get');
    this.action = this.jas_set.attr_get('action', '');
    this.target = this.jas_set.attr_get('target', '');
    this.form = _.new_element('form', {
        'method': this.method,
        'action': this.action,
        'target': this.target,
    });
    while (this.childNodes.length) {
        this.form.appendChild(this.firstChild);
    }
    this.appendChild(this.form);
    this.enabled = this.jas_set.attr_get('enabled', 'true') == 'true' ? true : false;
    this.type = this.jas_set.attr_get('type', 'native');
    var self = this;
    this.submit = function (e) {
        e.preventDefault();
        self.jas_set.$submit();
    };
    this.form.addEventListener('submit', this.submit);
}, 'form');

_.proc('enable', function () {
    this.enabled = true;
}, 'form');

_.proc('disable', function () {
    this.enabled = false;
}, 'form');

_.func('$submit', function () {
    var self = this;
    if (!this.enabled) {
        return;
    }
    self.jas_set.event_fire('prepare');
    var a = this.jas_set.find('*');
    var els = _();
    var r = /^jas-.+/i;
    for (var i = 0; i < a.length; i++) {
        if (r.test(a[i].nodeName)) {
            els.push(a[i]);
        }
    }
    var b = false;
    switch (els.length) {
        case 0:
            break;
        case 1:
            b = [els.is_input()];
            break;
        default:
            b = els.is_input();
            break;
    }
    var ok = true;
    for (var i = 0; i < els.length; i++) {
        if (!b[i]) {
            continue;
        }
        var t2 = _(els[i]);
        if (t2.required()) {
            if (!t2.test()) {
                t2.error();
                if (ok) {
                    t2.hint();
                }
                ok = false;
            }
        }
    }
    if (ok) {
        self.jas_set.event_fire('process');
        self.enabled = false;
        if (self.type === 'native') {
            self.form.submit();
        } else {
            var m = self.method;
            var a = self.action;
            _.ajax({
                url: a,
                method: m,
                data: new FormData(self.form),
                formdata: true,
                'result': function (t, s) {
                    self.jas_set.event_fire('result', [t, s]);
                }
            });
        }
    } else {
        self.jas_set.event_fire('error');
    }
    return ok;
}, 'form');

_.func('submit', function () {
    this.jas_set.event_fire('submit');
    this.jas_set.$submit();
}, 'form');

/*
_.proc('kill', function() {
    this.addEventListener('submit', this.submit);
    this.jas_inherit();
}, 'form');
*/
