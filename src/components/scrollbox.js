_.type('scrollbox');

_.proc('created', function () {
    this.jas_inherit();
    this.jas_set.class_add('jas-scrollbox');
    var self = this;
    this.margin = parseInt(this.jas_set.attr_get('margin', '5'));
    this.visibility = parseInt(this.jas_set.attr_get('visibility', '1'));
    this.mouseover = function (e) {
        self.jas_set.$mouseover(e);
    };
    this.mouseout = function (e) {
        self.jas_set.$mouseout(e);
    };
    this.wheel = function (e) {
        self.jas_set.$wheel(e);
    };
    this.mousedown = function (e) {
        self.jas_set.$mousedown(e);
    };
    this.docmousemove = function (e) {
        self.jas_set.$docmousemove(e);
    };
    this.docmouseup = function (e) {
        self.jas_set.$docmouseup(e);
    };
    this.touchstart = function (e) {
        self.jas_set.$touchstart(e);
    };
    this.doctouchmove = function (e) {
        self.jas_set.$doctouchmove(e);
    };
    this.doctouchend = function (e) {
        self.jas_set.$doctouchend(e);
    };
    this.addEventListener('mouseover', this.mouseover);
    this.addEventListener('mouseout', this.mouseout);
    this.addEventListener('wheel', this.wheel);
    this.addEventListener('touchstart', this.touchstart);
    this.sy = _.new_element('div', {
        class: 'jas_scrollbox_sy'
    }, this);
    this.sy.addEventListener('mousedown', this.mousedown);
    this.sy.style.right = this.margin + 'px';
    this.sy.style.top = this.margin + 'px';
    this.syb = _.new_element('div', {
        class: 'jas_scrollbox_syb'
    }, this.sy);
    if (this.visibility < 2) {
        this.sy.style.opacity = 0;
    }
    this.ds = 0;
    this.jas_set.$update();
}, 'scrollbox');

_.proc('$mouseover', function (e) {
    if (this.ds === 0) {
        if (this.visibility === 1) {
            this.sy.style.opacity = 1;
        }
    } else {
        this.ds = 1;
    }
}, 'scrollbox');

_.proc('$mouseout', function (e) {
    if (this.ds === 0) {
        if (this.visibility === 1) {
            this.sy.style.opacity = 0;
        }
    } else {
        this.ds = 2;
    }
}, 'scrollbox');

_.proc('$wheel', function (e) {
    e.preventDefault();
    this.scrollTop += e.deltaY;
    this.jas_set.$update();
}, 'scrollbox');

_.proc('$mousedown', function (e) {
    if (e.button === 0) {
        this.ly = e.pageY;
        this.lt = this.sybt;
        this.ds = 1;
        this.jas_set.class_add('jas_active');
        _(doc.body).class_add('jas_lock');
        doc.addEventListener('mousemove', this.docmousemove);
        doc.addEventListener('mouseup', this.docmouseup);
    }
}, 'scrollbox');

_.proc('$docmousemove', function (e) {
    var t = e.pageY - this.ly + this.lt;
    if (t < 0) {
        t = 0;
    }
    if (t > (this.syh - this.sybh)) {
        t = this.syh - this.sybh;
    }
    this.scrollTop = t / (this.syh / this.scrollHeight);
    this.jas_set.$update();
}, 'scrollbox');

_.proc('$docmouseup', function (e) {
    this.jas_set.class_remove('jas_active');
    _(doc.body).class_remove('jas_lock');
    var ds = this.ds;
    this.ds = 0;
    switch (ds) {
        case 1:
            this.jas_set.$mouseover();
            break;
        case 2:
            this.jas_set.$mouseout();
            break;
    }
    doc.removeEventListener('mousemove', this.docmousemove);
    doc.removeEventListener('mouseup', this.docmouseup);
}, 'scrollbox');

_.proc('$touchstart', function (e) {
    this.jas_set.$mouseover();
    this.lty = e.touches[0].pageY;
    this.ltt = this.scrollTop;
    this.jas_set.class_add('jas_active');
    _(doc.body).class_add('jas_lock');
    doc.addEventListener('touchmove', this.doctouchmove);
    doc.addEventListener('touchend', this.doctouchend);
}, 'scrollbox');

_.proc('$doctouchmove', function (e) {
    e.preventDefault();
    var t = (this.lty - e.touches[0].pageY) + this.ltt;
    if (t < 0) {
        t = 0;
    }
    if (t > this.scrollheight) {
        t = this.scrollheight;
    }
    this.scrollTop = t;
    this.jas_set.$update();
}, 'scrollbox');

_.proc('$doctouchend', function (e) {
    this.jas_set.class_remove('jas_active');
    _(doc.body).class_remove('jas_lock');
    doc.removeEventListener('touchmove', this.doctouchmove);
    doc.removeEventListener('touchend', this.doctouchend);
}, 'scrollbox');

_.proc('$update', function (e) {
    var h = this.scrollHeight;
    var rh = this.clientHeight;
    var t = this.scrollTop;
    this.syh = rh - (this.margin * 2);
    this.sybh = this.syh * (rh / h);
    this.sybt = t * (this.syh / h);
    this.sy.style.height = this.syh + 'px';
    this.sy.style.top = (t + this.margin) + 'px';
    this.syb.style.height = this.sybh + 'px';
    this.syb.style.top = this.sybt + 'px';
    this.sy.style.display = this.sybh === this.syh ? 'none' : 'block';
}, 'scrollbox');

/*

_.proc('kill', function() {
    this.removeEventListener('mouseover', this.mouseover);
    this.removeEventListener('mouseout', this.mouseout);
    this.removeEventListener('wheel', this.wheel);
    this.removeEventListener('touchstart', this.touchstart);
    this.sy.removeEventListener('mousedown', this.mousedown);
    doc.removeEventListener('mousemove', this.docmousemove);
    doc.removeEventListener('mouseup', this.docmouseup);
    doc.removeEventListener('touchmove', this.doctouchmove);
    doc.removeEventListener('touchend', this.doctouchend);
    this.removeChild(this.sy);
    this.jas_set.class_remove('jas_active').class_remove('jas_scrollbox');
    _(doc.body).class_remove('jas_lock');
    this.jas_inherit();
}, 'scrollbox');

*/
