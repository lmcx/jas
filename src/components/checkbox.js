_.type('checkbox', 'input');

_.func('test', function () {
    return this.state == 1;
}, 'checkbox');

_.proc('created', function () {
    var self = this;
    this.jas_inherit();
    this.jas_set.class_add('jas_noselect').class_add('jas-checkbox');
    this.state = this.value = parseInt(this.value) ? 1 : 0;
    if (this.state) {
        this.jas_set.class_add('jas_checked');
    }
    this.jas_set.value_set(this.state);
    this.click = function () {
        self.state = self.state ? 0 : 1;
        self.jas_set.class_remove('jas_error').value_set(self.state);
        if (self.state) {
            self.jas_set.class_add('jas_checked');
        } else {
            self.jas_set.class_remove('jas_checked');
        }
    };
    this.addEventListener('click', this.click);
    this.svg = _.new_element('div', {class: 'jas_checkbox_b1'}, this);
    this.svg.innerHTML = '\
        <svg class="jas_checkbox_svg" viewbox="0 0 44 44">\
            <rect class="jas_checkbox_rect" x="2" y="2" rx="6" ry="6" width="40" height="40"></rect>\
            <path class="jas_checkbox_path" d="m11,23l10,10l13,-22"></path>\
        </svg>\
    ';
}, 'checkbox');

/*
_.proc('kill', function () {
    this.jas_set
        .class_remove('jas_noselect')
        .class_remove('jas_checked');
    this.removeEventListener('click', this.click);
    this.removeChild(this.svg);
    this.jas_inherit();
}, 'checkbox');
*/
