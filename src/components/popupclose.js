_.type('popupclose');

_.proc('created', function () {
    this.jas_inherit();
    this.jas_set.class_add('jas-popupclose');
    var self = this;
    this.fclick = function () {
        self.jas_set.click();
    };
    this.addEventListener('click', this.fclick);
}, 'popupclose');

_.proc('click', function () {
    this.jas_set.parent('.jas-popup').hide();
    this.jas_inherit();
}, 'popupclose');

/*
_.proc('$kill', function() {
    this._node.class_remove('jas_popupclose');
    this.node.removeEventListener('click', this.click);
    this.jas_inherit();
}, 'popupclose');
*/
