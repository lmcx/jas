_.type('popup');

_.proc('created', function () {
    this.jas_inherit();
    this.jas_set.class_add('jas-popup');
    var self = this;
    this.overlay_class = this.jas_set.attr_get('overlay_class', 'jas_style');
    this.overlay = _(_.new_element('div', {
        'class': 'jas_popup_overlay ' + this.overlay_class
    }, doc.body));
    this.fclick = function () {
        self.jas_set.hide();
    };
    this.overlay[0].addEventListener('click', this.fclick);
    this.resize_eh = -1;
}, 'popup');

_.proc('show', function () {
    var self = this;
    _(this.overlay, this.jas_set).class_add('jas_visible');
    this.resize_eh = this.jas_set.event_add('resize', function () {
        self.jas_set.$update();
    });
    this.jas_set.$update();
    this.jas_set.event_fire('show');
}, 'popup');

_.proc('hide', function () {
    _(this.overlay, this.jas_set).class_remove('jas_visible');
    this.jas_set.event_remove(this.resize_eh);
    this.resize_eh = -1;
    this.style.left = '';
    this.style.top = '';
    this.jas_set.event_fire('hide');
}, 'popup');

_.proc('$update', function () {
    this.style.left = (doc.body.offsetWidth / 2 - this.offsetWidth / 2) + 'px';
    this.style.top = ((doc.documentElement.scrollTop || doc.body.parentNode.scrollTop || doc.body.scrollTop) + (win.innerHeight / 2 - this.offsetHeight / 2)) + 'px';
}, 'popup');

/*
_.proc('$kill', function() {
    this.jas_set.class_remove('jas_popup');
    this.overlay[0].removeEventListener('click', this.click);
    if (this.resize_eh != -1) {
        this.jas_set.event_remove(this.resize_eh);
    }
    this.overlay.remove();
    this.jas_inherit();
}, 'popup');
*/
