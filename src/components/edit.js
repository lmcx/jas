_.type('edit', 'input');

_.proc('created', function () {
    var self = this;
    this.type = this.jas_set.attr_get('type', 'text');
    this.input_focus = function () {
        self.jas_set.class_remove('jas_error').class_add('jas_active');
    };
    this.input_blur = function () {
        self.jas_set.class_remove('jas_active');
    };
    this.jas_inherit();
    this.jas_set.class_add('jas-edit');
}, 'edit');

_.proc('$create_input', function () {
    var self = this;
    switch (this.type) {
        case 'textarea':
            this.input = _.new_element('textarea', {
                'name': self.name,
                'placeholder': self.placeholder,
                'class': 'jas_edit_input'
            }, this);
            this.input.value = this.value;
            break;
        default:
            this.input = _.new_element('input', {
                'name': self.name,
                'type': (self.type === 'password' ? 'password' : 'text'),
                'value': self.value,
                'placeholder': self.placeholder,
                'class': 'jas_edit_input'
            }, this);
            break;
    }
    if (this.type === 'email') {
        this.jas_set.required_regex_set('^((?:(?:(?:[a-zA-Z0-9][\\.\\-\\+_]?)*)[a-zA-Z0-9])+)\\@((?:(?:(?:[a-zA-Z0-9][\\.\\-_]?){0,62})[a-zA-Z0-9])+)\\.([a-zA-Z0-9]{2,6})$');
    }
    this.input.addEventListener('focus', this.input_focus);
    this.input.addEventListener('blur', this.input_blur);
}, 'edit');

_.proto('type', function () {
    switch (arguments.length) {
        case 0:
            return this.type_get();
            break;
        case 1:
            return this.type_set(arguments[0]);
            break;
    }
}, 'edit');

_.func('type_get', function () {
    return this.type;
}, 'edit');

_.proc('type_set', function (v) {
    this.type = v;
    this.value = this.input.value;
    this.input.removeEventListener('focus', this.input_focus);
    this.input.removeEventListener('blur', this.input_blur);
    _(this.input).remove();
    this.jas_set.$create_input();
}, 'edit');

/*
_.proc('kill', function() {
    this.input.removeEventListener('focus', this.input_focus);
    this.input.removeEventListener('blur', this.input_blur);
    this.jas_inherit();
}, 'edit');
*/
