_.type('window');

_.proc('created', function () {
    this.jas_inherit();
    this.jas_set.class_add('jas-window');
    var self = this;
    this.width = parseInt(this.jas_set.attr_get('width', '100'));
    this.height = parseInt(this.jas_set.attr_get('height', '70'));
    this.caption_height = parseInt(this.jas_set.attr_get('caption_height', '32'));
    this.border_width = parseInt(this.jas_set.attr_get('border_width', '4'));
    this.x = parseInt(this.jas_set.attr_get('x', '0'));
    this.y = parseInt(this.jas_set.attr_get('y', '0'));
    this.style.width = (this.width + this.border_width * 2) + 'px';
    this.style.height = (this.height + this.caption_height + this.border_width) + 'px';
    this.style.left = this.x + 'px';
    this.style.top = this.y + 'px';
    this.dht = 0;
    this.mousedown = function (e) {
        self.jas_set.$mousedown(e);
    };
    this.mousemove = function (e) {
        self.jas_set.$mousemove(e);
    };
    this.docmousemove = function (e) {
        self.jas_set.$docmousemove(e);
    };
    this.docmouseup = function (e) {
        self.jas_set.$docmouseup(e);
    };
    this.addEventListener('mousedown', this.mousedown);
    this.addEventListener('mousemove', this.mousemove);
    this.b1 = _.new_element('div', {
        class: 'jas_window_b1'
    }, this);
    this.b1.style.top = this.caption_height + 'px';
    this.b1.style.left = this.border_width + 'px';
    this.b1.style.right = this.border_width + 'px';
    this.b1.style.bottom = this.border_width + 'px';
    this.b2 = _.new_element('div', {
        class: 'jas_window_b2 jas_noselect'
    }, this);
    this.b2.addEventListener('click', function () {
        self.jas_set.close();
    });
    this.b2.innerHTML = '\
        <svg class="jas_window_b2_svg" viewbox="0 0 44 44">\
            <rect class="jas_window_b2_rect" x="2" y="2" rx="6" ry="6" width="40" height="40"></rect>\
            <path class="jas_window_b2_path" d="m12,12l20,20"></path>\
            <path class="jas_window_b2_path" d="m12,32l20,-20"></path>\
        </svg>\
    ';
    this.b3 = _.new_element('div', {
        class: 'jas_window_b3'
    }, this.b1);
    this.b4 = _.new_element('div', {
        class: 'jas_window_b4'
    }, this);
    this.jas_set.show();
}, 'window');

_.proc('show', function(w) {
    var self = this;
    setTimeout(function () {
        self.jas_set.class_add('jas_visible');
    }, 1);
}, 'window');

_.proc('width', function(w) {
    this.width = w;
    this.style.width = (this.width + this.border_width * 2) + 'px';
}, 'window');

_.proc('height', function(h) {
    this.height = h;
    this.style.height = (this.height + this.caption_height + this.border_width) + 'px';
}, 'window');

_.proc('fit', function(h) {
    this.jas_set
        .width(this.b3.offsetWidth)
        .height(this.b3.offsetHeight);
}, 'window');

_.proc('close', function () {
    var self = this;
    this.jas_set.class_remove('jas_visible');
    setTimeout(function () {
        self.jas_set.remove();
    }, 250);
}, 'window');

_.proc('center', function () {
    var w = this.clientWidth;
    var h = this.clientHeight;
    var pw = this.parentNode.clientWidth;
    var ph = this.parentNode.clientHeight;
    this.jas_set.set_pos((pw / 2) - (w / 2), (ph / 2) - (h / 2));
}, 'window');

_.proc('set_pos', function (x, y) {
    if (y < 0) {
        y = 0;
    } else {
        var my = this.parentNode.clientHeight - this.caption_height;
        if (y > my) {
            y = my;
        }
    }
    this.style.left = x + 'px';
    this.style.top = y + 'px';
    this.x = x;
    this.y = y;
}, 'window');

_.proc('html', function (html) {
    this.b3.innerHTML = html;
}, 'window');

_.proc('title', function (title) {
    this.b4.innerHTML = title;
}, 'window');

_.proc('$mousedown', function (e) {
    if (e.button === 0) {
        if (e.target === this || e.target === this.b4) {
            this.dht = this.jas_set.$hittype(e.pageX - this.offsetLeft, e.pageY - this.offsetTop - 30);
            if (this.dht !== 0) {
                this.dx = e.pageX;
                this.dy = e.pageY;
                this.lx = this.x;
                this.ly = this.y;
                _(this.parentNode).class_add('jas_lock');
                doc.addEventListener('mousemove', this.docmousemove);
                doc.addEventListener('mouseup', this.docmouseup);
            }
        }
    }
}, 'window');

_.func('$hittype', function (x, y) {
    var r = 0;
    if (y <= this.caption_height) {
        r = 1;
    }
    return r;
}, 'window');

_.proc('$mousemove', function (e) {
    if (this.dht === 0) {
        var h = 0;
        if (e.target === this || e.target === this.b4) {
            h = this.jas_set.$hittype(e.pageX - this.offsetLeft, e.pageY - this.offsetTop);
        }
        var c = 'default';
        switch (h) {
            case 1:
                c = 'move';
                break;
        }
        this.style.cursor = c;
    }
}, 'window');

_.proc('$docmousemove', function (e) {
    switch (this.dht) {
        case 1:
            this.jas_set.set_pos(e.pageX - this.dx + this.lx, e.pageY - this.dy + this.ly);
            break;
    }
}, 'window');

_.proc('$docmouseup', function (e) {
    this.dht = 0;
    _(this.parentNode).class_remove('jas_lock');
    doc.removeEventListener('mousemove', this.docmousemove);
    doc.removeEventListener('mouseup', this.docmouseup);
}, 'window');

/*
_.proc('$kill', function() {
    this.jas_set.class_remove('jas_window');
    this.removeEventListener('mousedown', this.mousedown);
    this.removeEventListener('mousemove', this.mousemove);
    doc.removeEventListener('mousemove', this.docmousemove);
    doc.removeEventListener('mouseup', this.docmouseup);
    this.removeChild(this.b1);
    this.removeChild(this.b2);
    this.removeChild(this.b4);
    this.jas_set.class_remove('jas_visible');
    _(doc.body).class_remove('jas_lock');
    this.jas_inherit();
}, 'window');
*/
