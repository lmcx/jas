_.type('input');

_.proc('created', function () {
    this.jas_inherit();
    this.jas_set.class_add('jas-input');
    this.name = this.jas_set.attr_get('name', '');
    this.hint_class = this.jas_set.attr_get('hint_class', '');
    this.value = this.jas_set.attr_get('value', '');
    this.placeholder = this.jas_set.attr_get('placeholder', '');
    this.required = this.jas_set.attr_get('required', 'false') == 'true' ? true : false;
    this.required_msg = this.jas_set.attr_get('required_msg', 'Error!');
    this.required_regex = this.jas_set.attr_get('required_regex', '[^\\s]+');
    this.jas_set.$create_input();
    this.jas_set.$create_hint();
}, 'input');

_.proc('$create_input', function () {
    var self = this;
    self.input = _.new_element('input', {
        'name': self.name,
        'type': 'hidden',
        'value': self.value
    }, this);
}, 'input');

_.proc('$create_hint', function () {
    var h = _.new_element('jas-hint', {}, this);
    this.hint = _(h).class_add(this.hint_class).text_set(this.required_msg);
}, 'input');

_.proto('required', function () {
    switch (arguments.length) {
        case 0:
            return this.required_get();
            break;
        case 1:
            return this.required_set(arguments[0]);
            break;
    }
});

_.func('required_get', function () {
    return this.required;
}, 'input');

_.proc('required_set', function (b) {
    this.required = b;
}, 'input');

_.proto('required_msg', function () {
    switch (arguments.length) {
        case 0:
            return this.required_msg_get();
            break;
        case 1:
            return this.required_msg_set(arguments[0]);
            break;
    }
});

_.func('required_msg_get', function () {
    return this.required_msg;
}, 'input');

_.proc('required_msg_set', function (s) {
    this.required_msg = s;
    this.hint.text(s);
}, 'input');

_.proto('required_regex', function () {
    switch (arguments.length) {
        case 0:
            return this.required_regex_get();
            break;
        case 1:
            return this.required_regex_set(arguments[0]);
            break;
    }
});

_.func('required_regex_get', function () {
    return this.required_regex;
}, 'input');

_.proc('required_regex_set', function (s) {
    this.required_regex = s;
}, 'input');

_.proto('placeholder', function () {
    switch (arguments.length) {
        case 0:
            return this.placeholder_get();
            break;
        case 1:
            return this.placeholder_set(arguments[0]);
            break;
    }
});

_.func('placeholder_get', function () {
    return this.placeholder;
}, 'input');

_.proc('placeholder_set', function (s) {
    this.placeholder = s;
    this.input.placeholder = s;
}, 'input');

_.proto('value', function () {
    switch (arguments.length) {
        case 0:
            return this.value_get();
            break;
        case 1:
            return this.value_set(arguments[0]);
            break;
    }
});

_.func('value_get', function () {
    return this.input.value;
}, 'input');

_.proc('value_set', function (v) {
    this.value = v;
    this.input.value = v;
}, 'input');

_.proto('name', function () {
    switch (arguments.length) {
        case 0:
            return this.name_get();
            break;
        case 1:
            return this.name_set(arguments[0]);
            break;
    }
}, 'input');

_.func('name_get', function () {
    return this.name;
}, 'input');

_.proc('name_set', function (v) {
    this.name = v;
    this.input.name = v;
}, 'input');

_.func('is_input', function () {
    return true;
}, 'input');

_.func('test', function () {
    var r = true;
    if (this.required) {
        var rx = new RegExp(this.required_regex);
        r = rx.test(this.input.value);
    }
    return r;
}, 'input');

_.proc('error', function () {
    this.jas_set.class_add('jas_error');
}, 'input');

_.proc('hint', function () {
    this.hint.show();
}, 'input');

/*
_.proc('kill', function() {
    this.hint.remove();
    this.removeChild(this.input);
    this.jas_inherit();
}, 'input');
*/
