_.type('hint');

_.proc('created', function () {
    this.jas_inherit();
    this.jas_set.class_add('jas-hint');
    var self = this;
    this.text = this.jas_set.attr_get('text', 'Hint!');
    this.textdiv = _.new_element('div', {
        'class': 'jas_hint_text'
    }, this);
    this.taildiv = _.new_element('div', {
        'class': 'jas_hint_tail'
    }, this);
    this.docmousedown = function (e) {
        self.jas_set.hide();
    };
    this.jas_set.text_set(this.text);
}, 'hint');

_.proc('show', function () {
    this.style.display = 'block';
    doc.addEventListener('mousedown', this.docmousedown);
}, 'hint');

_.proc('hide', function () {
    doc.removeEventListener('mousedown', this.docmousedown);
    this.style.display = 'none';
}, 'hint');

_.proto('text', function () {
    switch (arguments.length) {
        case 0:
            return this.text_get();
            break;
        case 1:
            return this.text_set(arguments[0]);
            break;
    }
});

_.proc('text_set', function (s) {
    this.text = s;
    this.textdiv.textContent = s;
}, 'hint');

_.func('text_get', function () {
    return this.text;
}, 'hint');

/*
_.proc('kill', function() {
    doc.removeEventListener('mousedown', this.docmousedown);
    this.jas_set.removeChild(this.textdiv);
    this.jas_set.removeChild(this.taildiv);
    this.jas_inherit();
}, 'hint');
*/
