_.type('trackbar', 'input');

_.proc('created', function () {
    var self = this;
    this.lx = null;
    this.lv = null;
    this.md = false;
    this.button_width = parseInt(this.jas_set.attr_get('button_width', 20));
    this.min = parseInt(this.jas_set.attr_get('min', 0));
    this.max = parseInt(this.jas_set.attr_get('max', 100));
    this.step = parseInt(this.jas_set.attr_get('step', 1));
    this.line = _.new_element('div', {
        class: 'jas_trackbar_line'
    }, this);
    this.button = _.new_element('div', {
        class: 'jas_trackbar_button'
    }, this.line);
    this.mousedown = function (e) {
        if (e.button === 0) {
            self.jas_set.$mousedown(e);
        }
    };
    this.docmouseup = function (e) {
        if (e.button === 0) {
            self.jas_set.$docmouseup(e);
        }
    };
    this.docmousemove = function (e) {
        self.jas_set.$docmousemove(e);
    };
    this.addEventListener('mousedown', this.mousedown);
    this.jas_inherit();
    this.jas_set.class_add('jas-trackbar');
    if (this.value > this.max) {
        this.value = this.max;
    }
    if (this.value < this.min) {
        this.value = this.min;
    }
    this.jas_set.$update();
}, 'trackbar');

_.proc('$mousedown', function (e) {
    if ((e.target === this) || (e.target === this.line)) {
        this.jas_set.$hit(e.offsetX);
    }
    this.lx = e.pageX;
    this.lv = this.line.offsetWidth;
    this.md = true;
    this.jas_set.class_add('jas_active');
    _(doc.body).class_add('jas_lock');
    doc.addEventListener('mouseup', this.docmouseup);
    doc.addEventListener('mousemove', this.docmousemove);
}, 'trackbar');

_.proc('$docmouseup', function (e) {
    this.md = false;
    this.jas_set.class_remove('jas_active');
    this.jas_set.$docmousemove(e);
    _(doc.body).class_remove('jas_lock');
    doc.removeEventListener('mouseup', this.docmouseup);
    doc.removeEventListener('mousemove', this.docmousemove);
}, 'trackbar');

_.proc('$docmousemove', function (e) {
    this.jas_set.$hit((e.pageX - this.lx) + this.lv);
}, 'trackbar');

_.proc('$update', function () {
    var m = this.clientWidth - this.button_width;
    var f = this.max - this.min;
    var x = m * ((this.value - this.min) / f) + this.button_width / 2;
    this.line.style.width = x + 'px';
}, 'trackbar');

_.proc('$hit', function (x) {
    var b = this.button_width / 2;
    var m = this.offsetWidth - b;
    if (x < b) {
        x = b;
    }
    if (x > m) {
        x = m;
    }
    x -= b;
    m -= b;
    var f = this.max - this.min;
    var k = f * (x / m);
    k = Math.round(k / this.step) * this.step;
    var v = this.min + k;
    this.value = v;
    this.input.value = v;
    this.jas_set.$update();
    this.jas_set.event_fire('change', [v]);
}, 'trackbar');

_.proc('value_set', function (v) {
    if (v < this.min) {
        v = this.min;
    }
    if (v > this.max) {
        v = this.max;
    }
    this.value = v;
    this.input.value = v;
    this.jas_set.$update();
    this.jas_set.event_fire('change', [v]);
}, 'trackbar');

_.proto('min', function () {
    if (arguments.length) {
        return this.min_set(arguments[0]);
    } else {
        return this.min_get();
    }
}, 'trackbar');

_.proc('min_set', function (v) {
    this.min = v;
    this.jas_set.$update();
}, 'trackbar');

_.func('min_get', function () {
    return this.min;
}, 'trackbar');

_.proto('max', function () {
    if (arguments.length) {
        return this.max_set(arguments[0]);
    } else {
        return this.max_get();
    }
}, 'trackbar');

_.proc('max_set', function (v) {
    this.max = v;
    this.jas_set.$update();
}, 'trackbar');

_.func('max_get', function () {
    return this.max;
}, 'trackbar');

_.proto('step', function () {
    if (arguments.length) {
        return this.step_set(arguments[0]);
    } else {
        return this.step_get();
    }
}, 'trackbar');

_.proc('step_set', function (v) {
    this.step = v;
    this.jas_set.$update();
}, 'trackbar');

_.func('step_get', function () {
    return this.step;
}, 'trackbar');

_.proto('$button_width', function () {
    if (arguments.length) {
        return this.$button_width_set(arguments[0]);
    } else {
        return this.$button_width_get();
    }
}, 'trackbar');

_.proc('$button_width_set', function (v) {
    this.button_width = v;
    this.jas_set.$update();
}, 'trackbar');

_.func('$button_width_get', function () {
    return this.button_width;
}, 'trackbar');

/*
_.proc('kill', function() {
    this.removeEventListener('mousedown', this.mousedown);
    doc.removeEventListener('mouseup', this.docmouseup);
    doc.removeEventListener('mousemove', this.docmousemove);
    this.removeChild(this.line);
    this.jas_set.class_remove('jas_active');
    _(doc.body).class_remove('jas_lock');
    this.jas_inherit();
}, 'trackbar');
*/
