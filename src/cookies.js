var cookies = null;

_.cookies_update = function () {
    cookies = {};
    var pairs = doc.cookie.split(';');
    var pair;
    for (var i = 0; i < pairs.length; i++) {
        pair = pairs[i];
        while (pair.charAt(0) === ' ') {
            pair = pair.substring(1);
        }
        pair = pair.split('=');
        cookies[pair[0]] = pair[1];
    }
};

_.cookie_get = function (name, default_value) {
    name = escape(name);
    if (cookies === null) {
        _.cookies_update();
    }
    if (typeof default_value === 'undefined') {
        default_value = null;
    }
    if (typeof cookies[name] === 'undefined') {
        return default_value;
    } else {
        return unescape(cookies[name]);
    }
};

_.cookie_set = function (name, value, expires_timestamp, path, domain, secure) {
    name = escape(name);
    value = escape(value);
    if (cookies === null) {
        _.cookies_update();
    }
    cookies[name] = value;
    var s = name + '=' + value;
    if (typeof expires_timestamp !== 'undefined')
        s += '; expires=' + ((new Date(expires_timestamp)).toGMTString());
    if (typeof path !== 'undefined')
        s += '; path=' + path;
    if (typeof domain !== 'undefined')
        s += '; domain=' + domain;
    if (typeof secure !== 'undefined')
        s += '; secure';
    doc.cookie = s;
};

_.cookie_remove = function (name) {
    name = escape(name);
    if (cookies === null) {
        _.cookies_update();
    }
    delete cookies[name];
    doc.cookie = name + '=; expires=Thu, 01 Jan 1970 00:00:00 UTC';
};
