_.proc('hide', function() {
    this.style.display = 'none';
});

_.proc('show', function() {
    this.style.display = 'block';
});

_.proto('parent', function (selector) {
    if (!this.length) {
        return _();
    }
    var cur = this[0];
    if (typeof selector === 'undefined') {
        if (cur.parentNode) {
            return _(cur.parentNode);
        } else {
            return _();
        }
    }
    var all = doc.querySelectorAll(selector);
    var r = null;
    while (true) {
        cur = cur.parentNode;
        if (cur === null) {
            break;
        }
        for (var i = 0; i < all.length; i++) {
            if (all[i] === cur) {
                r = cur;
                break;
            }
        }
        if (r !== null) {
            break;
        }
    }
    if (r === null) {
        return _();
    } else {
        return _(r);
    }
});

_.func('attr_get', function (attr, def) {
    var a = this.getAttribute(attr);
    if (a !== null) {
        return a;
    }
    return (typeof def !== 'undefined') ? def : '';
});

_.proc('attr_set', function (attr, value) {
    this.setAttribute(attr, value);
});

_.proc('attr_remove', function (attr) {
    this.removeAttribute(attr);
});

_.proto('class_exists', function (className) {
    if (!this.length) {
        return false;
    }
    for (var i = 0; i < this.length; i++) {
        var b = this[i].className && (new RegExp('(^|\\s)' + className + '(\\s|$)')).test(this[i].className);
        if (b) {
            break;
        }
    }
    return b;
});

_.proc('class_remove', function (className) {
    var n = '';
    var cs = this.className.split(' ');
    for (var j = 0; j < cs.length; j++) {
        if (cs[j] !== className) {
            if (n !== '')
                n += ' ';
            n += cs[j];
        }
    }
    this.className = n;
});

_.proc('class_add', function (className) {
    var cs = this.className.split(' ');
    var b = false;
    for (var j = 0; j < cs.length; j++) {
        if (cs[j] === className) {
            b = true;
            break;
        }
    }
    if (!b) {
        this.className += (this.className === '' ? '' : ' ') + className;
    }
});

_.proto('find', function (selector, first) {
    var n = _();
    if (!this.length) {
        return n;
    }
    if (typeof selector === 'undefined') {
        selector = '*';
    }
    for (var i = 0; i < this.length; i++) {
        var a = this[i].querySelectorAll(selector);
        for (var j = 0; j < a.length; j++) {
            n.push(a[j]);
            if (first) {
                return n;
            }
        }
    }
    return n;
});

_.proto('children', function (selector) {
    var n = _();
    if (!this.length) {
        return n;
    }
    if (typeof selector === 'undefined') {
        selector = '*';
    }
    for (var i = 0; i < this.length; i++) {
        var a = this[i].children;
        for (var j = 0; j < a.length; j++) {
            n.push(a[j]);
        }
    }
    if (typeof selector === 'undefined') {
        return n;
    } else {
        return n.filter(selector);
    }
});

_.proto('first', function () {
    if (!this.length) {
        return _();
    }
    return _(this[0]);
});

_.proto('last', function () {
    if (!this.length) {
        return _();
    }
    return _(this[this.length - 1]);
});

var events = {};
var eventsx = {};
var events_i = 0;
var eventsx_i = 0;

_.proto('event', function () {
    switch (arguments.length) {
        case 1:
            this.event_fire(arguments[0]);
            break;
        case 2:
            this.event_add(arguments[0], arguments[1]);
            break;
    }
});

_.func('event_add', function (event, fn) {
    var eh = typeof this.jas_events === 'undefined' ? 0 : this.jas_events;
    if (eh === 0) {
        events_i++;
        eh = events_i;
        events[eh] = {};
        this.jas_events = eh;
    }
    if (typeof events[eh][event] === 'undefined') {
        events[eh][event] = [];
    }
    if (event === 'resize') {
        window.addEventListener(event, fn);
    } else {
        this.addEventListener(event, fn);
    }
    eventsx_i++;
    var ehx = eventsx_i;
    events[eh][event].push(fn);
    eventsx[ehx] = [this, event, fn];
    return ehx;
});

_.proc('event_remove', function (ev) {
    switch (typeof ev) {
        case 'string':
            var eh = typeof this.jas_events === 'undefined' ? 0 : this.jas_events;
            if (typeof events[eh][ev] !== 'undefined') {
                for (var i = 0; i < events[eh][ev].length; i++) {
                    if (ev === 'resize') {
                        window.removeEventListener(ev, events[eh][ev][i]);
                    } else {
                        this.removeEventListener(ev, events[eh][ev][i]);
                    }
                }
                delete events[eh][ev];
            }
            for (var e in eventsx) {
                if (eventsx.hasOwnProperty(e)) {
                    if (eventsx[e][0] === this && eventsx[e][1] === ev) {
                        delete eventsx[e];
                    }
                }
            }
            break;
        default:
            var a = eventsx[ev];
            var eh = typeof this.jas_events === 'undefined' ? 0 : this.jas_events;
            if (a[0] === this && eh !== 0) {
                var b = events[eh][a[1]];
                for (var i = 0; i < b.length; i++) {
                    if (b[i] === a[2]) {
                        events[eh][a[1]].splice(i, 1);
                        if (!events[eh][a[1]].length) {
                            delete events[eh][a[1]];
                        }
                        break;
                    }
                }
                if (a[1] === 'resize') {
                    window.removeEventListener(a[1], a[2]);
                } else {
                    this.removeEventListener(a[1], a[2]);
                }
                delete eventsx[ev];
            }
            break;
    }
});

_.func('event_fire', function (event, args) {
    var r = [];
    var eh = typeof this.jas_events === 'undefined' ? 0 : this.jas_events;
    if (eh !== 0) {
        if (typeof events[eh][event] !== 'undefined') {
            for (var i = 0; i < events[eh][event].length; i++) {
                r.push(events[eh][event][i].apply(this, args));
            }
        }
    }
    switch (r.length) {
        case 0:
            return null;
        case 1:
            return r[0];
        default:
            return r;
    }
});

_.proc('each', function (fn, a) {
    fn.apply(this, a);
});

_.proto('insert_before', function (element) {
    if (this.length) {
        this[0].parentNode.insertBefore(element, this[0]);
    }
    return this;
});

_.proto('insert_after', function (element) {
    if (this.length) {
        var n = this[0].nextSibling;
        if (n) {
            this[0].parentNode.insertBefore(element, n);
        } else {
            this[0].parentNode.appendChild(element);
        }
    }
    return this;
});

_.proc('html', function (html) {
    this.innerHTML = html;
});

_.proto('remove', function (html) {
    for (var i = 0; i < this.length; i++) {
        this[i].parentElement.removeChild(this[i]);
    }
    this.length = 0;
    return this;
});

_.proto('filter', function (s) {
    var r = _();
    var a = doc.querySelectorAll(s);
    for (var i = 0; i < a.length; i++) {
        if (this.indexOf(a[i]) !== -1) {
            r.push(a[i]);
        }
    }
    return r;
});

_.proto('not', function (t) {
    if (t.tagName) {
        while (true) {
            var i = this.indexOf(t);
            if (i !== -1) {
                this.splice(i, 1);
            } else {
                break;
            }
        }
    } else {
        var f = this.filter(t);
        for (var i = 0; i < f.length; i++) {
            this.splice(this.indexOf(f[i]), 1);
        }
    }
    return this;
});

_.proc('style', function () {
    if (!this.style) {
        return;
    }
    switch (arguments.length) {
        case 1:
            var arr = arguments[0];
            for (var k in arr) {
                if (arr.hasOwnProperty(k)) {
                    this.style[k] = arr[k];
                }
            }
            break;
        case 2:
            var a = arguments[0];
            var b = arguments[1];
            this.style[a] = b;
            break;
    }
});
