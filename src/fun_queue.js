var fun_run = false;
var fun_queue = [];
var is_ready = false;

function fun_queue_next() {
    if (is_ready) {
        if (!fun_run) {
            fun_run = true;
            while (fun_queue.length) {
                fun_queue[0]();
                fun_queue.shift();
            }
            fun_run = false;
        }
    }
}

function fun_queue_push(f) {
    fun_queue.push(f);
    fun_queue_next();
}

doc.addEventListener('DOMContentLoaded', function () {
    doc.removeEventListener('DOMContentLoaded', arguments.callee, false);
    is_ready = true;
    fun_queue_next();
});
