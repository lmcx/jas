_.version = version;
_.proto = jasset.prototype;
_.Sizzle = jas_Sizzle;
win.jas = _;
if (typeof win._ === 'undefined') {
    win._ = _;
}
win['jas-7'] = _;
win['jas-' + version] = _;

var types = {};

_.type = function (type, parent) {
    type = type.toUpperCase();
    var o = Object.create(HTMLElement.prototype);
    o.createdCallback = function() {
        this.jas_inherit_stack = [];
        this.jas_inherit = function () {
            return this.jas_inherit_stack[this.jas_inherit_stack.length - 1].apply(null, arguments);
        };
        this.jas_set = _(this);
        this.jas_set.created();
    };
    o.attachedCallback = function() {
        _(this).attached()
    };
    o.detachedCallback = function() {
        _(this).detached();
    };
    o.attributeChangedCallback = function(attr, old_value, new_value) {
        _(this).attr_changed(attr, old_value, new_value);
    };
    o.jas_type = type;
    doc.registerElement('jas-' + type.toLowerCase(), {
        prototype: o,
    });
    if (typeof types[type] !== 'undefined') {
        return false;
    }
    if (typeof parent === 'undefined') {
        parent = 'NODE';
    } else {
        parent = parent !== null ? parent.toUpperCase() : null;
    }
    types[type] = [parent, {}];
    return true;
};

function jasset_extend(name, func) {
    jasset.prototype[name] = func;
}

function meth_chain(type, proc_name) {
    var r = [];
    var p = type.toUpperCase();
    while (true) {
        if (typeof types[p] === 'undefined') {
            console.log('JAS: Can\'t get the chain - there\'s no type "' + p + '"');
            break;
        }
        if (typeof types[p][1][proc_name] !== 'undefined') {
            r.push(types[p][1][proc_name]);
        }
        p = types[p][0];
        if (p === null) {
            break;
        }
    }
    return r;
}

function meth_do(d, r, u, g) {
    if (g) {
        var s = [];
    }
    for (var i = 0; i < d.length; i++) {
        var type = typeof d[i].jas_type === 'undefined' ? 'NATIVE' : d[i].jas_type;
        var c = meth_chain(type, r);
        if (c.length) {
            var cur = -1;
            var v = null;
            if (type !== 'NATIVE') {
                d[i].jas_inherit_stack.push(function () {
                    cur++;
                    if (cur >= c.length) {
                        return;
                    }
                    return c[cur].apply(d[i], arguments);
                });
                v = d[i].jas_inherit.apply(d[i], u);
                if (d[i]) {
                    d[i].jas_inherit_stack.pop();
                }
            } else {
                v = c[0].apply(d[i], u);
            }
        } else {
            v = null;
        }
        if (g) {
            s.push(v);
        }
    }
    if (g) {
        switch (s.length) {
            case 0:
                return null;
            case 1:
                return s[0];
            default:
                return s;
        }
    }
}

_.proto = function (proto_name, proto) {
    jasset_extend(proto_name, proto);
};

_.proc = function (proc_name, proc, type) {
    if (typeof type === 'undefined') {
        type = 'NATIVE';
    } else {
        type = type.toUpperCase();
    }
    if (typeof types[type] === 'undefined') {
        console.log('JAS: Can\'t set the PROC - there\'s no type "' + type + '"');
        return;
    }
    types[type][1][proc_name] = proc;
    jasset_extend(proc_name, function () {
        meth_do(this, proc_name, arguments, false);
        return this;
    });
};

_.func = function (func_name, func, type) {
    if (typeof type === 'undefined' || type == null) {
        type = 'NATIVE';
    } else {
        type = type = type.toUpperCase();
    }
    if (typeof types[type] === 'undefined') {
        console.log('JAS: Can\'t set the FUNC - there\'s no type "' + type + '"');
        return;
    }
    types[type][1][func_name] = func;
    jasset_extend(func_name, function () {
        return meth_do(this, func_name, arguments, true);
    });
};

_.type('native', null);
_.type('node', 'native');

_.proc('created', function () {});
_.proc('created', function() {
    this.jas_inherit();
    if (this.className == '') {
        this.className = 'jas_style';
    }
}, 'node');
_.proc('attached', function() {});
_.proc('detached', function() {});
_.proc('attr_changed', function(attr, old_value, new_value) {});
