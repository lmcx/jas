(function (win, doc) {

    var version = '<%= pkg.version %>';
    var get_params = null;

    include "fun_queue.js"

    var jasset = function (es) {
        if (typeof es !== 'undefined') {
            if (!es.tagName) {
                for (var i = 0; i < es.length; i++) {
                    this.push(es[i]);
                }
            } else {
                this.push(es);
            }
        }
    };

    jasset.prototype = new Array();

    var jas_Sizzle = null;
    include "sizzle/sizzle.js"

    include "document-register-element/document-register-element.max.js"

    (function (_) {

        include "core.js"
        include "utils.js"
        include "native.js"
        include "components.js"
        include "css.js"

    })(function () {
        var p = [];
        for (var i = 0; i < arguments.length; i++) {
            var t = typeof arguments[i];
            if (t === 'string') {
                var q = jas_Sizzle(arguments[i]);
                for (var j = 0; j < q.length; j++) {
                    p.push(q[j]);
                }
            } else if (t === 'function') {
                if (is_ready) {
                    t();
                } else {
                    fun_queue_push(arguments[i]);
                }
            } else if (arguments[i].tagName) {
                p.push(arguments[i]);
            } else if (arguments[i] instanceof Array) {
                for (var j = 0; j < arguments[i].length; j++) {
                    p.push(arguments[i][j]);
                }
            }
        }
        return new jasset(p);
    });

})(window, document);
